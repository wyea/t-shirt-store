T-Shirt Store
=============

You can find [this app on Heroku](https://morning-brook-28217.herokuapp.com/) (if you're lucky, as I have a free Heroku account).

### Admin user:
*   email: admin@example.com
*   password: qwerty

### Regular user:
*   email: user@example.com
*   password: qwerty
