class AddItemNumberToVariations < ActiveRecord::Migration[5.0]
  def change
    add_column :variations, :item_number, :string
    add_index :variations, :item_number, unique: true
  end
end
