class CreateVariations < ActiveRecord::Migration[5.0]
  def change
    create_table :variations do |t|
      t.belongs_to :product, index: true
      t.string :size
      t.string :color
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
