class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :title
      t.string :maker
      t.integer :price_cents
      t.text :description

      t.timestamps null: false
    end
  end
end
