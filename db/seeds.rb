colors = { "black"    => File.open(Rails.root + "public/images/black.jpg"),
           "blue"     => File.open(Rails.root + "public/images/blue.jpg"),
           "green"    => File.open(Rails.root + "public/images/green.jpg"),
           "grey"     => File.open(Rails.root + "public/images/grey.jpg"),
           "orange"   => File.open(Rails.root + "public/images/orange.jpg"),
           "pink"     => File.open(Rails.root + "public/images/pink.jpg"),
           "red"      => File.open(Rails.root + "public/images/red.jpg"),
           "sky_blue" => File.open(Rails.root + "public/images/sky_blue.jpg"),
           "white"    => File.open(Rails.root + "public/images/white.jpg"),
           "yellow"   => File.open(Rails.root + "public/images/yellow.jpg") }
sizes = %w(3XS XXS XS S M L XL XXL 3XL)
item_number_allowed_characters = ("A".."Z").to_a - %w(I L O)
24.times do
  title = Faker::Commerce.product_name.gsub(/(?<=\s)\w+$/, "T-Shirt")
  maker = Faker::Company.name.split(//).first(20).join("")
  price = Faker::Commerce.price
  description = Faker::Lorem.paragraph.split(//).first(256).join("")
  picked_colors = colors.to_a.sample(4).flatten.each_slice(2).to_h
  picked_sizes  = sizes.sample(2)
  item_number_symbols = item_number_allowed_characters.sample(3).join("")
  product = Product.create!(title:       title,
                            maker:       maker,
                            price:       price,
                            description: description,
                            pictures:    [picked_colors.values[0],
                                          picked_colors.values[1],
                                          picked_colors.values[2],
                                          picked_colors.values[3]])
  picked_sizes.each_with_index do |size, index|
    3.times do |n|
      item_number = item_number_symbols + "0" + index.to_s + (n + 1).to_s
      color = picked_colors.keys[n]
      quantity = rand(10)
      product.variations.create!(item_number: item_number, size: size,
                                 color: color, quantity: quantity)
    end
  end
end
AdminUser.create!(email: "admin@example.com", password: "password",
                  password_confirmation: "password")
User.create!(email: "user@example.com", password: "password",
             password_confirmation: "password")
