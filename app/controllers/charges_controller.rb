# app/controllers/charges_controller.rb
class ChargesController < ApplicationController
  def new
  end

  def create
    @amount = params[:stripeAmount]
    @email  = params[:stripeEmail]
    @card   = params[:stripeToken]

    begin
      @amount = @amount.to_i # Must be an integer!
    rescue
      flash[:error] = "Charge not completed. Please enter a valid amount."
      redirect_to cart_path
      return
    end

    customer = Stripe::Customer.create(
      email: @email,
      card: @card
    )

    Stripe::Charge.create(
      amount: @amount,
      currency: "aud",
      customer: customer.id,
      receipt_email: @email,
      description: "Charge for #{@email}"
    )

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to cart_path
    flash[:notice] = "Please try again!"
  end
end
