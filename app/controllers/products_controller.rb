# app/controllers/products_controller.rb
class ProductsController < ApplicationController
  before_action :admin_user, except: [:index, :show]
  before_action :find_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.order(:title).page params[:page]
  end

  def show
  end

  def new
    @product = Product.new
    @product.variations.build
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to product_path(@product)
    else
      render "new"
    end
  end

  def edit
  end

  def update
    if @product.update(product_params)
      redirect_to product_path(@product)
    else
      render "edit"
    end
  end

  def destroy
    @product.destroy
    redirect_to root_path
  end

  private

    def product_params
      params.require(:product).permit(:title, :maker, :price, :description,
                                      pictures: [],
                                      variations_attributes:
                                        [:item_number, :size, :color,
                                         :quantity, :_destroy, :id])
    end

    def find_product
      @product = Product.find(params[:id])
    end

    def admin_user
      redirect_to root_path unless user_signed_in? && current_user.try(:admin?)
    end
end
