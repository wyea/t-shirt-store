# app/controllers/carts_controller.rb
class CartsController < ApplicationController
  before_action :authenticate_user!

  def index
    # If there is a cart, display it. Otherwise, pass an empty value.
    return @cart = session[:cart] if session[:cart]
    @cart = {}
  end

  def add
    id = params[:id]
    # Create a new cart, if a cart is not already created.
    session[:cart] ||= {}
    cart = session[:cart]

    # If a product is already in the cart, increment the quantity value.
    cart[id] ||= 0
    cart[id] += 1
    redirect_to cart_path
  end

  def clear_cart
    session[:cart] = nil
    redirect_to action: :index
  end
end
