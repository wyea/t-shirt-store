# app/models/product.rb
class Product < ApplicationRecord
  has_many :variations, dependent: :destroy
  accepts_nested_attributes_for :variations, allow_destroy: true

  mount_uploaders :pictures, PictureUploader
  paginates_per 12

  monetize :price_cents

  before_save { self.price_cents = price_cents.to_i }

  validates :title,       presence: true, length: { maximum: 32 }
  validates :maker,       presence: true, length: { maximum: 20 }
  validates :description, presence: true
  validates :price_cents, presence: true,
                          numericality: { greater_than_or_equal_to: 0,
                                          only_integer: true }
end
