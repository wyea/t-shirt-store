# app/models/variation.rb
class Variation < ApplicationRecord
  belongs_to :product

  before_save { self.quantity = quantity.to_i }

  scope :unique_by_size,  -> { select(:size).uniq  }
  scope :unique_by_color, -> { select(:color).uniq }

  # Accepts only 3XS XXS XS S M L XL XXL 3XL
  VALID_SIZE_REGEX = /\A[3X]?[X]?((?<!3)[SL]|(?<!\w)[M])\z/
  validates :size,     presence: true, length: { maximum: 3 },
                       format: { with: VALID_SIZE_REGEX }
  validates :color,    presence: true, length: { maximum: 20 }
  validates :quantity, presence: true,
                       numericality: { greater_than_or_equal_to: 0 }
  # First three characters are letters with exception of I, L and O,
  # next three characters are digits
  VALID_ITEM_NUMBER_REGEX = /\A[A-HJKMNP-Z]{3}\d{3}\z/
  validates :item_number, presence: true, length: { maximum: 6 },
                          format: { with: VALID_ITEM_NUMBER_REGEX },
                          uniqueness: { case_sensitive: false }
end
