// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).ready(() => {
  $('#variation_color').change(() => {
    const colorChosen = $('#variation_color :selected').text();
    $('#color_chosen').html(colorChosen);
  });
  $('#variation_size').change(() => {
    const sizeChosen = $('#variation_size :selected').text();
    $('#size_chosen').html(sizeChosen);
  });
});
