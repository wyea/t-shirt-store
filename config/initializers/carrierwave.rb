# config/initializers/carrierwave.rb
CarrierWave.configure do |config|
  config.aws_credentials = {
    access_key_id:          ENV["AWS_ACCESS_KEY_ID"],
    secret_access_key:      ENV["AWS_SECRET_ACCESS_KEY"],
    region:                 ENV["S3_REGION"],
    endpoint:               ENV["S3_ENDPOINT"]
  }

  # Upload files to:
  # AWS (Amazon S3) for production,
  # local public folder for development,
  # local tmp folder for testing.
  if Rails.env.production?
    config.storage = :aws
  else
    config.storage = :file
    config.enable_processing = false
    config.root = if Rails.env.development?
                    "#{Rails.root}/public"
                  else
                    "#{Rails.root}/tmp"
                  end
  end

  config.aws_bucket = ENV["S3_BUCKET_NAME"]
  config.cache_dir  = "#{Rails.root}/tmp/uploads"
  config.aws_acl    = "public-read"
end
