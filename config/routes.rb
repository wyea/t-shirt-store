Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, path_names: { sign_in: "login", sign_out: "logout",
                                   sign_up: "register" }
  resources :products
  root "products#index"

  resources :charges, only: [:new, :create]

  get "/cart"       => "carts#index"
  get "/cart/clear" => "carts#clear_cart"
  get "/cart/:id"   => "carts#add"
end
