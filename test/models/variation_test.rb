# test/models/variation_test.rb
require "test_helper"
# Variation model test
class VariationTest < ActiveSupport::TestCase
  def setup
    @variation = Variation.new(item_number: "SKU123",
                               size: "XS", color: "White", quantity: 5)
  end

#  test "should be valid" do
#    assert @variation.valid?
#  end

  test "size should be present" do
    @variation.size = "   "
    assert_not @variation.valid?
  end

  test "color should be present" do
    @variation.color = "   "
    assert_not @variation.valid?
  end

  test "quantity should be present" do
    @variation.quantity = "    "
    assert_not @variation.valid?
  end

  test "item number should be present" do
    @variation.item_number = "    "
    assert_not @variation.valid?
  end

  test "size should not be too long" do
    @variation.size = "XXXXS"
    assert_not @variation.valid?
  end

  test "color should not be too long" do
    @variation.color = "W" + "a" * 20
    assert_not @variation.valid?
  end

  test "item number should not be too long" do
    @variation.item_number = "SKU3245"
    assert_not @variation.valid?
  end

#  test "size validation should accept valid size names and values" do
#    valid_sizes = %w(3XS XXS XS S M L XL XXL 3XL)
#    valid_sizes.each do |valid_size|
#      @variation.size = valid_size
#      assert @variation.valid?,
#             "#{valid_size.inspect} should be valid"
#    end
#  end

  test "size validation should reject invalid values" do
    invalid_sizes = %w(3xs xxs xs s l xl xxl x 3m 35 LL 3M 3S 3L
                       LXL 3xl xS 2XS m Medium 3)
    invalid_sizes.each do |invalid_size|
      @variation.size = invalid_size
      assert_not @variation.valid?, "#{invalid_size.inspect} should be invalid"
    end
  end

#  test "item_number validation should accept valid values" do
#    valid_item_numbers = %w(AEY001 NMP651 HTR427 BKU643
#                            ADD101 NKE021 PUM024 AUS127)
#    valid_item_numbers.each do |valid_item_number|
#      @variation.item_number = valid_item_number
#      assert @variation.valid?, "#{valid_item_number.inspect} should be valid"
#    end
#  end

  test "item_number validation should reject invalid values" do
    invalid_item_numbers = %w(NIK001 nik101 H9K765 not889 MJHY06 NN5432 KJU_08
                              KJYTG98765 654378 Nk654 LOK123 MNB12Y JHU12O
                              aey001 nmp651 htr427 add101 nke021 pum024)
    invalid_item_numbers.each do |invalid_item_number|
      @variation.item_number = invalid_item_number
      assert_not @variation.valid?,
                 "#{invalid_item_number.inspect} should be invalid"
    end
  end

  test "item number addresses should be unique" do
    duplicate_variation = @variation.dup
    duplicate_variation.item_number = @variation.item_number
    @variation.save
    assert_not duplicate_variation.valid?
  end
end
