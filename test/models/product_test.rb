# test/models/product_test.rb
require "test_helper"
# Product Model Tests
class ProductTest < ActiveSupport::TestCase
  def setup
    @product = Product.new(title: "Example T-shirt", maker: "Adidas",
                           price_cents: "3795",
                           description: "The best T-Shirt you've ever had")
  end

  test "should be valid" do
    assert @product.valid?
  end

  test "title should be present" do
    @product.title = "    "
    assert_not @product.valid?
  end

  test "maker should be present" do
    @product.maker = "   "
    assert_not @product.valid?
  end

  test "price should be present" do
    @product.price_cents = ""
    assert_not @product.valid?
  end

  test "description should be present" do
    @product.description = "   "
    assert_not @product.valid?
  end

  test "title should not be too long" do
    @product.title = "a" * 33
    assert_not @product.valid?
  end

  test "maker should not be too long" do
    @product.maker = "a" * 21
    assert_not @product.valid?
  end

  test "price should be a number" do
    @product.price_cents = "twenty"
    assert_not @product.valid?
  end

  test "price validation should accept valid values" do
    valid_price_values = %w(95 1595 1200 15 799 1 12 120 9898)
    valid_price_values.each do |valid_price_value|
      @product.price_cents = valid_price_value
      assert @product.valid?, "#{valid_price_value.inspect} should be valid"
    end
  end

  test "price validation should reject invalid values" do
    invalid_price_values = %w(one 1. 2.aa 2.4563 14.95 d.95 4r.15 one)
    invalid_price_values.each do |invalid_price_value|
      @product.price_cents = invalid_price_value
      assert_not @product.valid?,
                 "#{invalid_price_value.inspect} should be invalid"
    end
  end
end
